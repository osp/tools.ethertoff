To theme the wiki, you can override any of the files in the ethertoff/static
folder by placing them in the static folder of this example_theme app.

For example, if you want to have a new stylesheet, override ethertoff/static/css/style.css
by creating a file static/css/style.css here.
